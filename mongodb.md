# Bases de datos con MongoDB

MongoDB es un gestor de bases de tados en tiempo real, muy rapida y facil de usar. Su modelo es No solo SQL\(NoSQL\), que a diferencia de los modelos de las bases de datos SQL, como MySql, postgres, Oracle, las bases de datos NoSql tienen mayor flexibilidad a la hora de insercar campor. Esta flexibilidad hacer que sea mucho mas rapido y escalable. MongoDB realiza sus querys obtenido sus objetos en formato .JSON:

En Mongo se utiliza terminologia o conceptos diferente a las bases tradicionales SQL:

| Bases SQL | Bases NoSql MongoDB |
| :--- | :--- |
| Tables | Collections |
| rows | documents\(BJSON\) |
| columns | fields |
|  |  |

### Instalacion

### Crear bases de datos

Para la ceracion de bases de datos es facil, solo utilizmos el siguiente comando:

```
// para la creacion de bases de datos
> use <name_data_base>
switched to db <name_data_base>

// verificar su creacion
>db
<name_data_base>
```

Simplemente con ese comando iniciamos, per aun asi no tenemos completo el proceso, para que quede definitivamente creada debemos crea una coleccion dentro de esta base de datos:

```
// Primero seleccionamos la base de datos a utilizar
> use nameDB
> db.createCollection("nameCollection")

// Verificar la coleccion creada
> show collections
nameCollection
```

### Usuarios y Roles

Para la creacion de ususarios en mongo se hace uso de la funcion **db.createUser\(user\), **si el nuevo usuario a crear ya existe, regresa un error indicando este problema. Un ejemplo de como se crea un usuario con sus diferentes roles es el siguiente:

```
// Seleccinamos la base de datos a utilizar
> use admin
> db.createUser(
   {
     user: "appAdmin",
     pwd: "password",
     roles:
       [
         role: "readWrite",
         "clusterAdmin" // "dbAdmin"
       ]
   }
)
```

### Insertar documentos

Como se lo dijo anteriormente, los documentos es la infomacion estructurada que gurdamos en base de datos, es lo que en SQL se conoce como rows. Para realizar este proceso solo debemos hacer lo siguiente:

```
// insercion de documento
> db.namecollection.insert({_id:23, field1:"datos", field2:"datos", field3:34})

// Insercion de varios documentos
> db.namecollection.insert([{datos...}, {datos...}])
```

el field **\_id **es un campo requerido y representa el valor unico en cada documento, si en una insercion no se lo indica, mongo automaticamente crea el registro y le asigna una valor.

### Querys

En mongo para buscar los valore en una base de datos utilizamos la funcion db.namecollection.find\({data...}\). En data  van los criterios de busqueda especificando el valor buscado en la correspondiente field:

```
> db.namecollection.find({_id: 12, name:'jose'})
// se busca en la coleccion namecollection el documento con id 12 y nombre jose
```

Esto debera retornar informacion en tipo json.

