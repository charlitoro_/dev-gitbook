# Generación de certificados SSL con Let's Encrypt

#### ¿ Qué es el protocolo HTTPS y los certificados SSL ?

Cuando nuestro sitio web utiliza el protocolo HTTP este se encuentra vulnerable a intercepción de información que se transmite entre el servidor y el cliente. El protocolo HTTPS nos permite establecer una conexión segura, encriptando los datos que se transmiten. Los certificados SSL \( Secure Sockets Layer\) son un título digital que cifra la información y permiten el paso, como un tipo de pasaporte, estos certificados generan unas llaves que validan la conexión y encriptan la información que se trasfiere entre el servidor y el cliente.

![](/assets/protocolo https.png)

#### ¿ Qué es Let's Encrypt ?

Let's Encrypt es una entidad de certificación, la cual nos permite generar certificados SSL para habilitar HTTPS en páginas web de forma automática, gratuita y segura. Está respaldada por una gran [comunidad](https://letsencrypt.org/) de grandes empresas tecnológicas. Let's Encrypt cuenta con varios clientes para poder trabajar a través de consola en nuestros servidores, ellos recomiendan a [cerbot](https://certbot.eff.org/) por su amplia documentación y su compatibilidad con muchos sistemas operativos. Para nuestro ejemplo vamos a utilizar cerbot, para ello tenemos que instalar el cliente dependiendo del sistema operativo, solo ingresa a su [pagina](https://certbot.eff.org/) y selecciona el servidor web y el sistema operativo.

#### Tutorial generación de certificados SSL con Let's Encrypt

Digital Ocean es un servicio que provee servidores privados, llamados **droplet**, los cueles se pueden configurar según su capacidad de almacenamiento, memoria RAM y servicios adicionales que pueden ser adquiridos por un [costo](https://www.digitalocean.com/pricing/) adicional. Los DNS de nuestros dominios tambien pueden ser configurados en digital ocean, este post lo puedes encontrar aqui.

Para este tutorial vamos a utilizar un droplet de digital ocean con ubuntu 18.04.

Lo primero que debemos hacer es instalar el plugin DNS de cerbot en el servidor. Cerbot nos ofrece una lista de [plugins](https://certbot.eff.org/docs/using.html#dns-plugins) dependiendo de donde tenemos configurados los DNS de nuestro dominio.

```
$ pip3 install certbot-dns-digitalocean
```

Para poder acceder a nuestra cuenta de digital ocean necesitamos crear un archivo con las credenciales que se obtienen en el apartado de API,  [Aplicaciones y Token](https://cloud.digitalocean.com/settings/api/tokens). El directorio donde guardes estas credenciales debe ser un lugar seguro y oculto:

```
## ruta donde se guardara las credenciales
$ mkdir ~/.secret/
$ nano ~/.secret/digital-credential.ini

## contenido del archivo con el token generado
dns_digitalocean_token = 0000111122223333444455556666777788889999aaa
```

##### Generación de los certificados:

Los certificados son dos archivos, uno nombrado como **fullchain.pem **y **privkey.pem**, estos se generan fácilmente con un solo comando. Es común que queramos una certificación para nuestros subdominios como **docs.example.com, api.example.com, admin.example.com** etc. las credenciales generadas servirán para todos los subdominios que tengamos, solo hay que utilizar el comodín " **\*. "**, además de esto hay que configurar los **CNAME** en digital ocean \(post aqui\):

```
## Especificamos el directorio donde se guardo el archivo .ini con las credenciales
## asi como tambien el dominio que esta relacionado al droplet en digital ocean

$ sudo certbot certonly \
> --dns-digitalocean \
> --dns-digitalocean-credentials ~/.secret/digital-credentials.ini \
> -d "*." -d example.com --server https://acme-v02.api.letsencrypt.org/directory
```

Los certificados** **quedan guardados en la ruta **/etc/letsencrypt/live/example.com/. **Con las llaves generadas solo nos resta configurar nuestro servbidor web, como **nginx o apache** para ver los resultados.

##### Renovacion de certificados:

Los certificados generados tienen una duracion de 90 dias, pero no hay de que preocuparse, con un simple comando puedemos renovar nuestros certificados por otros 90 dias. Solo se debe ejecutar el comando:

```
$ sudo certbot renew --dry-run
```

Esto debera sobreescribir las llaves y tendriamos que volver a configurar en nuestro servidor web. Esta configuración de servidores web la puede encontrar en este post.

### Tipos de Challenges para la generacion de los certificados SSL con el cliente Certbot.

### Generación de certificados SSL con HTTP-01 Challenge - Cliente Certbot

Un Challenge es la manera como se verifican y generan los certificados SSL con el cliente Certbot. DNS-01 challenge es la comúnmente utilizada para la generación de nuestros certificados SSL, esto lleva una configuración de nuestros DNS en el host donde se tenga instalado nuestro cliente Certbot, sin embargo, puede suceder que no tengamos acceso para configurar los DNS del dominio se quiere certificar. Certbot nos ofrece una alternativa que es HTTP-01 Challenge, nos permite generar nuestros certificados usando el directorio local en el servidor donde este ubicado nuestro sitio web, esto nos libra de tener que configurar nuestros DNS.

Para la generación utilizaremos el complemento webroot, Intenta colocar un archivo donde pueda ser servido sobre HTTP por el puerto 80, a través de un servidor web como Apache o Nginx. Use el complemento Webroot cuando ejecute Certbot en un servidor web con cualquier aplicación escuchando en el puerto 80 que sirve archivos de una carpeta local como respuesta. Antes de realizar el proceso se debe verificar los siguiente:

* Que el nombre del dominio existe y que este apuntando a una dirección ip publica configurada por un servidor web.
* Tener instalado el cliente Certbot \([https://certbot.eff.org/](https://certbot.eff.org/)\)
* Que el puerto 80 esté abierto y que exista un servidor web escuchando por este puerto.
* Tener el directorio donde se encuentra nuestro sitio web ejemplo **/var/www/example.com**.

El comando que debemos ejecutar es muy simple, acceden como superusuario en su servidor y ejecutan:

```
# certbot certonly --webroot -w /var/www/example.com -d example.com
```

Esto nos creara un directorio **/etc/letsencrypt/live/example.com/ **donde encontramos el certificado **fullchain.pem **y **privkey.pem, **solo resta configurar estas llaves en nuestro servidor web ya sea Apache o Nginx.

Hay una limitación al realizar la generación de los certificados con HTTP-01 Challenge, el certificado solo es válido para los dominios especificados en el comando ejecutado:

```
# certbot certonly --webroot -w /var/www/example.com -d example.com -d docs.example.com -w /var/www/other.com -d other.com
```

Este comando generara un solo certificado para todo los dominio y subdominios especificados \([documentación](https://certbot.eff.org/docs/using.html#getting-certificates-and-choosing-plugins)\), esto significa que si queremos utilizar el mismo certificado para todos los subdominios de nuestro dominio \(utilizando el comodín \*\) no será posible, además todos estos dominios y subdominios deben existir y estar configurados en un servidor web.

