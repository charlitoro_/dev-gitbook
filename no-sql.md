# Bases de Datos NoSQL

# ![](/assets/nosql.jpg)

El primer error que de comete cuando escuchamos el termino **NoSQL** es llegar a la conclusion que este tipo de bases de datos son totalmente desentendidas del lenguaje de consulta **SQL** \(Strunctured Query Languege\), pero esto no es asi. El termino **NoSQL** realmente significa _**Not Only SQL,**_ lo cual nos dice que que son bases de datos que no solo son **SQL**, que existen otras maneras de estrucurar la información.

La principal diferencia que existre en las bases de datos **SQL** y **NoSQL** es la relacion que existe entre sus entidades, las bases de datos **SQL** son consideradas relacionales, compuestas por tablas que estan organizadas en columnas, cada columna almacena un tipo de dato en especifico \(integer, real, number, string, date, etc.\) y cada fila representa una instancia o registro de la tabla. Las bases de datos **NoSQL** son consideradas **no relacionales **no almacenan los datos en tablas, en lugar de eso hay multiples formas de alamcenar los datos las bases de datos **NoSQL**. Actualemente existen 4 formas de alamacenar la información, **Key-value stores, Document databases, Column stores, Graph databases**.

![](/assets/sql-nosql-esquema-base-datos.png)

### Key-Values stores

La estructura de los datos en las bases de datos **key-value **esta dado por medio de diccionarios, un valor se puede almacenar como un entero, una cadena, JSON, o una matriz, con una clave que se utiliza para hacer referencia a ese valor, por ejemplo, podria tener la identificacion del cliente como clave, que se refiere a un valor que contiene una cadena del nombre del cliente.

Estas tipo de bases de datos se caracterizan por er muy rapidas, en su almacenamiento, esto es debido a su alta simplisidad, ademas tiene una funcion de alamcenamiento en cache lo que permite a los usuarios almacenar y recuperar datos lo mas rapido posible. Por todas esas caracteristicas las bases de datos Key-Values son consideradas las mas simples en el mundo de las bases de datos NoSQL.

![](/assets/redisdb.png)![](/assets/amazon-dynamo-db.png)

### Document databases

Los datos en este tipo de bases de datos es almacenada en forma de documentos y coleciones, un documento puede ser un archivo PDF, Word, XML o JSON \(JSON es el formato mas utilizado\). A diferencia de las columnas y los tipos de datos un documento contiene pares de valores clave. Cada documento no tiene que estar en la misma estructura que otros documentos. Debido a esto, para agregar datos adicionales, simplemente puede agregar más documentos sin tener que cambiar la estructura de la base de datos completa el modelado de datos flexible \(elimina la necesidad de forzar el ajuste de modelos de datos relacionales, ya que puede manejar estructurados, datos no estructurados y semiestructurados\) y rendimiento de escritura rápida sobre una consistencia estricta \(ideal para Agile y iteración rápida\).

### ![](/assets/mongoDB.png)![](/assets/Couchbase logo.gif)

### Column store

La forma como las bases de datos orientadas a columnas almacenan datos es, como su nombre lo dice, en columnas agrupadas en lugar de filas de datos. utiliza un concepto llamado llamado espacio de teclado, que es similar al esquema de un modelo relacional. El espacio de claves contiene múltiples familias de columnas. Las familias de columnas son similares a las tablas en un modelo relacional. Tienen una llave de fila y otra de columnas para hacer consultas muy rápidas y guardar grandes cantidades de información pero modelar los datos se puede volver un poco complicado. Las usamos en Big Data, IoT, sistemas de recomendaciones, entre otras

![](/assets/casandra.png)![](/assets/HBase.png)

Referencia: [https://medium.com/@mark.rethana/introduction-to-nosql-databases-c5b43f3ca1cc](https://medium.com/@mark.rethana/introduction-to-nosql-databases-c5b43f3ca1cc)

### Graph databses

Bases de datos basadas en Grafos. Nos permiten establecer conexiones entre nuestras entidades para realizar consultas de una forma más eficiente que en bases de datos relacionales. Es un sistema de administracion de bases de datos en line con metodos Create, Read, Update and Delete\(CRUD\) que expone un modelo de datos en grafos. Las bases de datos basadas en grafosgeneralemnte se crean para uso con sistemas transaccionales\(OLTP\), como tambien para estructuras complejas donde existen muchas relaciones entre las entidades, este tipo de bases de datos nos permitiran acceder en corto tiempo a la información. En consecuencia, normalmente están optimizados para el rendimiento transaccional y se diseñan teniendo en cuenta la integridad transaccional y la disponibilidad operativa.

![](/assets/logo_neo4j.png)![](/assets/janusgraph.png)

Referencia: [https://pdfs.semanticscholar.org/f511/7084ca43e888fb3e17ab0f0e684cced0f8fd.pdf](https://pdfs.semanticscholar.org/f511/7084ca43e888fb3e17ab0f0e684cced0f8fd.pdf)

## Pros y Contras





