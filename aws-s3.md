# AWS S3 servicio de almacenamiento

S3 es un servicio de Amazon que nos permite crear espacio para almacenamiento estatico de información.

## Instalación y configuración cliente AWS

## Creacion de buckes por consolo

El siguiente codigo bash retorna 0 si el bucket existe y 255 si no existe

```
aws s3 ls s3://bucket/filname
if [[ $? -ne 0 ]]; then
  echo "File does not exist"
fi
```

En S3 los nombres de los buckets son unicos, no solo en nuestras cuentas sino unicos entre todos los bukets existentes en AWS. En ese sentido si al ejecutar la linea:

```
$ aws s3api create-bucket --bucket name-bucket --region us-east-1
```

nos arroja un error es porque el bucket ya existe en alguna cuenta, retornara un codigo 255. Ahora si el bucket creado no existe y es correcta la ejecucion debera reguesar un codigo 0 y por consola retornara un json con la ruta del bucket.

```
{
    "Location": "/name-bucket"
}
```

## Habilitar el alojamiento de sitio web estatico

S3 cuenta con un gran servicio de alojameinto de sitios web estaticos, el comando para poder activar esta opcion es la siguiente:

```
$ aws s3api put-bucket-website --bucket www.mybucket.com --website-configuration file://website.json

website.json:
{
    "IndexDocument": {
        "Suffix": "index.html"
    },
    "ErrorDocument": {
        "Key": "error.html"
    }
}
```

## Cambio de politicas de un Bucket

Las politicas son esos permisos de acceso que tiene un bucket en AWS.

```
$ aws s3api put-bucket-policy --bucket MyBucket --policy file://policy.json

policy.json: 
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::MyBucket/*"
        }
    ]
}
```

## Subir un archivo a un bucket

Ahora vamos a ver como se suben archivos estaticos a un bucket de S3 por medio de la consola:

```
$ aws s3api put-object --bucket text-content --key index.html --body index.html --content-type text/html
```



